﻿using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Services;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
    public class GivePromoCodesToCustomersConsumer : IConsumer<GivePromoCodeToCustomerMessage> //GivePromoCodeRequest
    {
        private readonly IPromoCodeService _promoCodeService;
        public GivePromoCodesToCustomersConsumer(IPromoCodeService promoCodeService)
        {
            _promoCodeService = promoCodeService;
        }
        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerMessage> context)
        {
            await _promoCodeService.GivePromoCodesToCustomersWithPreferenceAsync(context.Message);
        }
    }
}
