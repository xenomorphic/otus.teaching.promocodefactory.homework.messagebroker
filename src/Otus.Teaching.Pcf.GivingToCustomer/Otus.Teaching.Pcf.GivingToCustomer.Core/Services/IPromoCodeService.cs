﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public interface IPromoCodeService
    {
        public Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeToCustomerMessage message);
    }
}

