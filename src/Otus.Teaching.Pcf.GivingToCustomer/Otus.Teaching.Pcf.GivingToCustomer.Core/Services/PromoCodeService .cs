﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public class PromoCodeService : IPromoCodeService
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        public PromoCodeService(IRepository<PromoCode> promoCodesRepository,
            IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }
        public async Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeToCustomerMessage message)
        {
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(message.PreferenceId);
            if (preference == null)
                throw new System.Exception("Предпочтение не найдено");
            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));
            var promocode = new PromoCode();
            promocode.Id = message.PromoCodeId;
            promocode.PartnerId = message.PartnerId;
            promocode.Code = message.PromoCode;
            promocode.ServiceInfo = message.ServiceInfo;
            promocode.BeginDate = DateTime.Parse(message.BeginDate);
            promocode.EndDate = DateTime.Parse(message.EndDate);
            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;
            promocode.Customers = new List<PromoCodeCustomer>();

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {
                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            };

            await _promoCodesRepository.AddAsync(promocode);
        }
    }

}
