﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public class PartnerPromoCodeSerivce : IPartnerPromoCodeService
    {
        private readonly IRepository<Employee> _employeeRepository;
        public PartnerPromoCodeSerivce(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        public async Task UpdateAppliedPromocodesAsync(Guid? id)
        {
            if (id == null)
                throw new System.Exception("Менеджер не найден");
            var employee = await _employeeRepository.GetByIdAsync((Guid)id);
            if (employee == null)
                throw new System.Exception("Менеджер не найден");
            employee.AppliedPromocodesCount++;
            await _employeeRepository.UpdateAsync(employee);
        }
    }
}
