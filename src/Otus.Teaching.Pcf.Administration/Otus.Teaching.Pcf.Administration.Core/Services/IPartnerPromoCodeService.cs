﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public interface IPartnerPromoCodeService
    {
        public Task UpdateAppliedPromocodesAsync(Guid? id);
    }
}
